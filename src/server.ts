import App from './app';

const app = new App();

app.buildApp().then(server => {
  server.listen(
    process.env.PORT as string,
    process.env.HOST as string,
    (err: Error, address: string) => {
      if (err) {
        server.log.error(err);
        process.exit(1);
      }
    }
  );
});
