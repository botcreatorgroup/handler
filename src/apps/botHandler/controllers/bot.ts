import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { Update } from 'telegraf/typings/telegram-types';

export const handleAll = (fastify: FastifyInstance) => async (
  request: FastifyRequest<{ Params: { token: string }, Body: Update }>,
  reply: FastifyReply
) => {
  await fastify.handler.handleMessage(request, reply);
  // reply.status(200).send(200);
};
