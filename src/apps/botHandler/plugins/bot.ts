import { FastifyInstance, FastifyRegisterOptions } from 'fastify';
import { Update } from 'telegraf/typings/telegram-types';

import { handleAll } from '../controllers/bot';
import { BotHandler } from '../decorators/telegram';
import { botValidation } from '../hooks/preValidation';

const botPlugin = async (
  fastify: FastifyInstance,
  options: FastifyRegisterOptions<any>
) => {
  fastify.decorate('handler', new BotHandler(fastify));

  fastify.addHook('preValidation', botValidation(fastify));
  // fastify.addHook('preHandler', addBotUser(fastify));
  // here all handler handlers
  fastify.post<{
    Params: {
      token: string;
    };
    Body: Update;
  }>('/:token', handleAll(fastify));
};

export default botPlugin;
