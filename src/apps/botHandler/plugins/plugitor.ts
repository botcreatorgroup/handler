import botPlugin from './bot';
import { FastifyInstance, FastifyRegisterOptions } from 'fastify';


const handlerPlugin = async (fastify: FastifyInstance, options: FastifyRegisterOptions<any>) => {
  // Here we declare all handler plugins
  await fastify.register(botPlugin);
};

export default handlerPlugin;
