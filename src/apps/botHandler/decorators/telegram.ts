import { Telegraf } from 'telegraf';
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { TelegrafContext } from 'telegraf/typings/context';
import { Update } from 'telegraf/typings/telegram-types';
import { BotObject } from '../typings/bot';
import { LinkComponentModel, TextComponentModel } from '../typings/models';

export class BotHandler {
  private readonly fastify: FastifyInstance;

  constructor(fastify: FastifyInstance) {
    this.fastify = fastify;
  }

  public async handleMessage(
    request: FastifyRequest<{ Body: Update }>,
    reply: FastifyReply
  ): Promise<void> {
    // Extracting botModel which has been assigned in preValidation hook.

    const bot = new Telegraf<any>(request.data.bot.token);
    // Command is a text type, so we declare it above text handler
    // In future we will iterate through schema commands
    // TODO: create a Command model, which will be linked to a schema

    // This command is necessary
    bot.command('start', async (ctx: TelegrafContext) => {
      if (!request.data.schema) return;
      if (request.data.tabs) await this.redirectToTab(request.data.tabs[0].id, request.data, ctx);
    });

    await bot.handleUpdate(request.body);

    return reply.send({});
  }

  // As navigation we have 'text' message from telegram
  private async redirectToTab(
    tabId: number,
    data: BotObject,
    ctx: TelegrafContext
  ): Promise<void> {
    if (!data.tabs) return;
    const tab = data.tabs.filter(tab => tab.id === tabId)[0];
    this.fastify.log.debug(`building tab components for ${tab.id}`);
    // Iterate through components and send all available content
    if (!tab.components) return;
    for (const component of tab.components) {
      this.fastify.log.debug(`component #${component.id} ${component.type}`);

      if (component.type === 'text') {
        const metaComponent = component as TextComponentModel;
        await ctx.replyWithMarkdown(metaComponent.content);
      }

      if (component.type === 'link') {
        const metaComponent = component as LinkComponentModel;
        await this.redirectToTab(metaComponent.linkToNextTab, data, ctx);
        return;
      }

      // TODO: Write situations
      // if (model.type === 'image') continue;
      // if (model.type === 'document') continue;
    }
    // When tab has been send, we change current tab in session table
    // const sessionModel = new Session(this.fastify.pg);
    // if (ctx.message?.from?.id) {
    //   if (await sessionModel.isExist(ctx.message?.from?.id)) {
    //     await sessionModel.updateTabInSession(tabId, ctx.message?.from?.id);
    //   } else {
    //     await sessionModel.insertTabInSession(tabId, ctx.message?.from?.id);
    //   }
    // }
  }
}
