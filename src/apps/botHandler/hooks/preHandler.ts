// import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
// import { Update } from 'telegraf/typings/telegram-types';
// import { Bot } from '../../../global/models/Bot';
//
// export const addBotUser = (fastify: FastifyInstance) => async (
//   request: FastifyRequest<{ Body: Update, Params: { token: string } }>,
//   reply: FastifyReply
// ) => {
//   const botUserModel = new Bot(fastify.pg);
//   if (request.body.message?.from) {
//     if (!await botUserModel.isUserExist(request.body.message.from)) {
//       fastify.log.debug(`user ${request.body.message.from.id} does not exists.`);
//       await botUserModel.putUserToDb(request.body.message.from, request.params.token);
//       return;
//     }
//     fastify.log.debug(`user ${request.body.message.from.id} already in database`);
//   }
// };
