import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { BotObject } from '../typings/bot';

export const botValidation = (fastify: FastifyInstance) => async (
  request: FastifyRequest<{ Params: { token: string } }>,
  response: FastifyReply
) => {
  const redisData = await fastify.redis.get(request.params.token);
  if (!redisData) return;
  const botData = JSON.parse(redisData) as BotObject;
  if (!botData || !botData.bot.isRunning) {
    fastify.log.debug(`bot is incorrect or has been rejected`);

    // #TODO Send an error in a manner of { code, status, message }
    response.send(400);
    // For efficiency
  } else {
    request.data = botData;
    fastify.log.debug(`preparing for handle ${botData.bot.token}`);
  }
};
