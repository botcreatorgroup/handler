import fastifyPlugin from 'fastify-plugin';
import fastifyRedis from 'fastify-redis';
import { FastifyInstance, FastifyRegisterOptions } from 'fastify';

const dbConnector = async (
  fastify: FastifyInstance,
  options: FastifyRegisterOptions<any>
) => {
  fastify.register(fastifyRedis, {
    host: process.env.REDIS_HOST,
    port: parseInt(<string>process.env.REDIS_PORT, 10)
  });
};

export default fastifyPlugin(dbConnector);
