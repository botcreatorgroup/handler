import fastifyPlugin from 'fastify-plugin';
import dbPlugin from './database';

import { FastifyInstance, FastifyRegisterOptions } from 'fastify';

const globalPlugin = async (fastify: FastifyInstance, options: FastifyRegisterOptions<any>) => {
  // Global plugins
  fastify.register(dbPlugin);
};

export default fastifyPlugin(globalPlugin);

