import fastify, { FastifyInstance, FastifyServerOptions } from 'fastify';
import * as dotenv from 'dotenv';
import { DotenvConfigOptions } from 'dotenv';
import { Telegraf } from 'telegraf';

// Plugins
import handlerPlugin from './apps/botHandler/plugins/plugitor';
import globalPlugin from './global/plugins/plugitor';

export default class {
  public app: FastifyInstance;

  constructor(opts: FastifyServerOptions = {}) {
    this.app = fastify(opts);
  }

  private async initPlugins() {
    // Global plugins
    await this.app.register(globalPlugin);


    // Handler plugins
    await this.app.register(handlerPlugin, {
      prefix: '/handler'
    });
  }

  private async initTelegraf(token: string) {
    const bot = new Telegraf(token);
    await bot.telegram.setWebhook('https://bots.cloud2y.com/handler/' + token);
  }

  public async buildApp() {
    let options: DotenvConfigOptions;
    if (process.env.NODE_ENV === 'DEV') {
      options = { path: '.env' };
      this.app.log.info('Development config is used');
    } else if (process.env.NODE_ENV === 'TEST') {
      options = { path: '.env.test' };
      this.app.log.info('Testing config is used');
    } else {
      this.app.log.error('Unknown configuration');
      process.exit(1);
    }

    dotenv.config(options);
    await this.initTelegraf('1209144004:AAH-jQdfzdyd9SSeYMX3UYW3l4tpJwsA5qo');
    await this.initPlugins();
    return this.app;
  }
}
