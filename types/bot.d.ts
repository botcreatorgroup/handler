import { BotObject } from '../src/apps/botHandler/typings/bot';
import { BotHandler } from '../src/apps/botHandler/decorators/telegram';

declare module 'fastify' {
  export interface FastifyRequest {
    data: BotObject;
  }

  export interface FastifyInstance {
    handler: BotHandler;
  }
}
