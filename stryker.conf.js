/**
 * @type {import('@stryker-mutator/api/core').StrykerOptions}
 */
module.exports = {
  packageManager: 'npm',
  reporters: ['html'],
  testRunner: 'command',
  mutate: ['./src/apps/api/**/*.ts'],
  coverageAnalysis: 'off'
};
